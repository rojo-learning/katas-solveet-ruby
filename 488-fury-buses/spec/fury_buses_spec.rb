
require File.expand_path('spec/spec_helper')

require 'pty'

ALL_SEATS      =  /(\[..\])/
FREE_SEATS     = /(\[\s\d\]|\[\d\d\])/
OCCUPIED_SEATS = /(\[\s\*\])/

RSpec.describe 'Fury Buses' do
  let(:buffer      ) { ""               }
  let(:program_end ) { "Ending program" }

  def run_program(&instructions)
    # Start program execution
    @output, @input = PTY.spawn(File.expand_path 'fury_buses.rb')
    # Pass instructions to program
    instructions.call if block_given?
    # Instruct the program to quit
    @input.puts '4'
    # Wait a little for the program
    sleep 0.5
    # Read console output so far
    @output.readpartial(1024, buffer) until buffer =~ /#{program_end}/
  end

  describe 'main menu' do
    it 'displays menu title' do
      message = 'BUS MENU'
      run_program
      expect(buffer).to match(message)
    end

    it 'displays assign seat option' do
      message = 'Assign Seat'
      run_program
      expect(buffer).to match(message)
    end

    it 'displays free seat option' do
      message = 'Free Seat'
      run_program
      expect(buffer).to match(message)
    end

    it 'displays show seats option' do
      message = 'Show Seats'
      run_program
      expect(buffer).to match(message)
    end

    it 'displays quit program option' do
      message = 'Quit Program'
      run_program
      expect(buffer).to match(message)
    end
  end

  describe 'seats' do
    it 'can be displayed' do
      message = '\[ \d\]|\[\d\d\]|\[ \*\]'
      run_program { @input.puts '3' }
      expect(buffer).to match(message)
    end

    it 'can be assigned if free' do
      seat = '1'
      # Look for the status of seat 1 referencing seat 2
      message = '\[ \*\]\[ 2\]'

      run_program do
        # Ensure that the seats are free
        @input.puts '2'
        @input.puts seat
        @input.puts '2'
        @input.puts "#{seat.to_i + 1}"
        # Assign seat
        @input.puts '1'
        @input.puts seat
        # Show seats status
        @input.puts '3'
      end

      expect(buffer).to match(message)
    end

    it 'can be freed if occupied' do
      seat = '1'
      # Look for the status of seat 1
      message = '\[ 1\]'

      run_program do
        # Ensure that the seat is occupied
        @input.puts '1'
        @input.puts seat
        # Free seat
        @input.puts '2'
        @input.puts seat
        # Show seats status
        @input.puts '3'
      end

      expect(buffer).to match(message)
    end

    it 'can not be assigned if not free' do
      seat = '10'
      message = "Seat #{seat} is not available."

      run_program do
        # Ensure that the seat is occupied
        @input.puts '1'
        @input.puts seat
        # Try to assign seat again
        @input.puts '1'
        @input.puts seat
        # Show seats status
        @input.puts '3'
      end

      expect(buffer).to match(message)
    end

    it 'can not be freed if free' do
      seat = '25'
      message = "Seat #{seat} is already available."

      run_program do
        # Ensure that the seat is free
        @input.puts '2'
        @input.puts seat
        # Try to free seat again
        @input.puts '2'
        @input.puts seat
        # Show seats status
        @input.puts '3'
      end

      expect(buffer).to match(message)
    end
  end

  describe 'configuration' do
    # These tests focus on the fixed configuration that reads on the program
    # description, but idealy the test should check the values provided in
    # /config/presets.json and ensure that the behavior of the program matches
    # what id described there.

    xit 'the max number of seats is respected'
    xit 'the random number of occupied seats is between the limits'

    it 'there are at most 40 seats' do
      run_program do
        # Show seats status
        @input.puts '3'
      end

      seats = buffer.scan(ALL_SEATS)
      expect(seats.size).to be <= 40
    end

    it 'between 5 and 30 random seats are occupied' do
      run_program do
        # Show seats status
        @input.puts '3'
      end

      seats = buffer.scan(OCCUPIED_SEATS)
      expect(seats.size.between? 5, 30).to be
    end
  end

end
