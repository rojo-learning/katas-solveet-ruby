
require File.expand_path('spec/spec_helper')
require File.expand_path('lib/presets')
require File.expand_path('lib/bus')

RSpec.describe Presets do

  describe '.load' do

    describe 'before loading configuration' do
      it '::VEHICLE is not initialized' do
        message = 'Presets::VEHICLE'
        expect { Presets::VEHICLE }.to raise_error(NameError, /#{message}/)
      end

      it '::OCCUPIED_SEAT_MEARK is not initialized' do
        message = 'Presets::OCCUPIED_SEAT_MARK'
        expect { Presets::OCCUPIED_SEAT_MARK }.to raise_error(NameError, /#{message}/)
      end
    end

    describe 'loading configuration' do
      describe 'with an existing file' do
        it 'loads the configuration without errors' do
          file = File.expand_path('spec/fixtures/config.json')
          expect { Presets.load file }.not_to raise_error
        end
      end

      describe 'with a non existing file' do
        it 'raises Errno::ENOENT error for the file' do
          RSpec::Expectations.configuration.on_potential_false_positives = :nothing

          fake_file = 'fake_file.json'
          expect { Presets.load fake_file }.not_to raise_error('Errno::ENOENT', /#{fake_file}/)
        end
      end
    end

    describe 'with configuration loaded' do
      let(:config) do
        {
          vehicle_class: "Bus",
          seats_configuration: [4, 4, 4, 4, 4, 4, 4, 4, 4, 4],
          occupied_seat_mark: "*",
          use_random_seats: true,
          min_taken_seats: 5,
          max_taken_seats: 30
        }
      end

      before do
        allow(JSON).to receive(:parse).and_return(config)
      end

      describe '::VEHICLE' do
        describe 'when use_random_seats is true' do
          it 'some seats are occupied' do
            $VERBOSE = nil
            Presets.load File.expand_path('spec/fixtures/config.json')
            $VERBOSE = true

            expect(Presets::VEHICLE.seats.any? { |_, v| v == :occupied }).to be
          end
        end

        describe 'when use_random_seats is false' do
          it 'all seats are available' do
            config[:use_random_seats] = false
            $VERBOSE = nil
            Presets.load File.expand_path('spec/fixtures/config.json')
            $VERBOSE = true

            expect(Presets::VEHICLE.seats.all? { |_, v| v == :available }).to be
          end
        end

        it 'is instance of the configured class' do
          vehicle_class = 'Bus'
          config[:vehicle_class] = vehicle_class
          $VERBOSE = nil
          Presets.load File.expand_path('spec/fixtures/config.json')
          $VERBOSE = true

          expect(Presets::VEHICLE.class.to_s).to eql(vehicle_class)
        end
      end

      describe '::OCCUPIED_SEAT_MARK' do
        it 'is the configured string' do
          occupied_seat_mark = '~'
          config[:occupied_seat_mark] = occupied_seat_mark
          $VERBOSE = nil
          Presets.load File.expand_path('spec/fixtures/config.json')
          $VERBOSE = true

          expect(Presets::OCCUPIED_SEAT_MARK).to eql(occupied_seat_mark)
        end
      end

    end

  end

end
