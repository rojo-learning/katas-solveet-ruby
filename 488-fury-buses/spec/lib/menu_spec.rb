
require File.expand_path('spec/spec_helper')
require File.expand_path('lib/menu')

class Dummy
  include Menu
end

RSpec.describe Menu do
  context 'when included' do
    describe 'target class' do
      it 'has Menu class available' do
        expect(Dummy::Menu).to be
      end
    end

    describe 'target instance' do
      subject { Dummy.new }

      it { should respond_to :require_input }
      it { should respond_to :exec_option   }

      describe '#require_input' do
        it 'presents the given input message to stdout' do
          allow(subject).to receive(:gets).and_return('some input')
          message  = 'gimme input!'

          expect { subject.require_input message }.to output(/#{message}/).to_stdout
        end

        it 'returns striped version of the user input' do
          user_input = 'some input '
          allow(subject).to receive(:gets).and_return(user_input)

          expect(subject.require_input '').to eql(user_input.chomp)
        end
      end

      describe '#exec_option' do
        let(:menu_title   ) { 'Menu Title'        }
        let(:menu_command ) { :nil?               }
        let(:menu_text    ) { 'Fake Command Text' }
        let(:menu_legend  ) { 'Menu Input Legend' }

        let(:my_menu) do
          Menu::Menu.new(
            title: menu_title,
            options: { 1 => { command: menu_command, text: menu_text } },
            input_legend: menu_legend
          )
        end

        it 'displays the menu input legend' do
          user_input = '1 '
          allow(subject).to receive(:gets).and_return(user_input)

          expect { subject.exec_option my_menu }.to output(/#{menu_legend}/).to_stdout
        end

        it 'the object issued the selected command' do
          user_input = '1 '
          allow(subject).to receive(:print).and_return('')
          allow(subject).to receive(:gets ).and_return(user_input)

          expect(subject).to receive(menu_command)
          expect { subject.exec_option my_menu }.not_to raise_error
        end
      end

    end
  end
end

RSpec.describe Menu::Menu do
  let(:menu_title) { 'Menu Title' }

  subject do
    Menu::Menu.new(
      title: menu_title,
      options: {
        1 => { command: :menu_command1, text: 'menu text 1' },
        2 => { command: :menu_command2, text: 'menu text 2' },
        3 => { command: :menu_command3, text: 'menu text 3' }
      },
      input_legend: 'menu input legend'
    )
  end

  it 'instances are immutable' do
    expect(subject.frozen?).to be
  end

  describe '#print' do
    it 'displays the menu title' do
      expect { subject.print }.to output(/#{menu_title}/).to_stdout
    end

    it 'displays all the menu options' do
      subject.options.each do |_, option|
        expect { subject.print }.to output(/#{option[:text]}/).to_stdout
      end
    end
  end

end
