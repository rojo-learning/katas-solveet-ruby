
require File.expand_path('spec/spec_helper')
require File.expand_path('lib/bus')

RSpec.describe 'Bus' do

  describe 'with valid configuration' do
    let(:configuration) { [2, 4, 4, 4, 4, 4, 4, 4, 4, 4] }
    let(:max_row_seats) { 9 }
    let(:min_row_seats) { 1 }
    let(:str_representation) { 'some string'.freeze }
    let(:fake_available_row_configs) do
      {
        min_row_seats => { str: str_representation, blocks_hallway: false },
        2             => { str: str_representation, blocks_hallway: false },
        3             => { str: str_representation, blocks_hallway: false },
        max_row_seats => { str: str_representation, blocks_hallway: true  }
      }
    end

    subject { Bus.new configuration }

    describe '#seats_number' do
      it 'returns the number of seats' do
        expected = 38
        expect(subject.seats_number).to eql expected
      end
    end

    describe '#seat_avaiable?' do
      it 'returns true with an available seat number' do
        seat_number = 1
        subject.seats[seat_number] = :available
        expect(subject.seat_available? seat_number).to be
      end

      it 'returns false with an unavailable seat number' do
        seat_number = 1
        subject.seats[seat_number] = :occupied
        expect(subject.seat_available? seat_number).not_to be
      end
    end

    describe '#assign_seat' do
      let(:seat_number) { 2                    }
      before { subject.assign_seat seat_number }

      it 'sets seat with given number as occupied' do
        expect(subject.seats[seat_number]).to eql :occupied
      end

      it 'assigned seat should not be available' do
        expect(subject.seat_available? seat_number).not_to be
      end
    end

    describe '#free_seat' do
      let(:seat_number) { 3                  }
      before { subject.free_seat seat_number }

      it 'sets seat with given number as available' do
        expect(subject.seats[seat_number]).to eql :available
      end

      it 'assigned seat should be available' do
        expect(subject.seat_available? seat_number).to be
      end
    end

    describe '#row_representation' do
      it 'returns a string representing a row with a given number of seats' do
        allow(subject).to receive(:available_row_configs)
          .and_return(fake_available_row_configs)
        expect(subject.row_representation max_row_seats).to eql str_representation
      end
    end

    describe '#max_row_seats' do
      it 'returns the maximum number of seats accepted for a row' do
        allow(subject).to receive(:available_row_configs)
          .and_return(fake_available_row_configs)
        expect(subject.max_row_seats).to eql max_row_seats
      end
    end

    describe '#min_row_seats' do
      it 'returns the minimum number of seats accepted for a row' do
        allow(subject).to receive(:available_row_configs)
          .and_return(fake_available_row_configs)
        expect(subject.min_row_seats).to eql min_row_seats
      end
    end

    it 'new instance has all its seats available' do
      subject.seats.each do |_, status|
        expect(status).to eql :available
      end
    end
  end

  describe 'with non valid configuration' do
    let(:fake_available_row_configs) do
      {
        2 => { str: str_representation, blocks_hallway: false },
        3 => { str: str_representation, blocks_hallway: false },
        4 => { str: str_representation, blocks_hallway: false },
        5 => { str: str_representation, blocks_hallway: true  }
      }
    end

    describe 'with a row that has number of seats lower than the limit' do
      it 'raises an argument exception' do
        faulty_configuration = [2, 4, 4, 4, 4, 0, 4, 4, 4, 4]
        expect { Bus.new faulty_configuration }.to raise_error ArgumentError
      end
    end

    describe 'with a row that has number of seats bigger than the limit' do
      it 'raises an argument exception' do
        faulty_configuration = [2, 4, 4, 9, 4, 4, 4, 4, 4, 4]
        expect { Bus.new faulty_configuration }.to raise_error ArgumentError
      end
    end

    describe 'with a row configuration that blocks the walkway' do
      it 'raises an argument exception' do
        faulty_configuration = [2, 5, 4, 4, 4, 4, 4, 4, 4, 4]
        expect { Bus.new faulty_configuration }.to raise_error ArgumentError
      end
    end
  end
end
