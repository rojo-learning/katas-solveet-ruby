
module Menu

  def require_input(input_legend)
    print "#{input_legend} "
    gets.chomp
  end

  def exec_option(menu)
    selection = require_input(menu.input_legend).to_i
    self.send menu.options[selection][:command]
  end

  class Menu
    attr_reader :title, :options, :input_legend

    def initialize(title:, options:, input_legend:)
      @title = title
      @options = options
      @input_legend = input_legend

      self.freeze
    end

    def print
      puts title
      options.each { |selector, option| puts "#{selector}) #{option[:text]}" }
    end
  end

end
