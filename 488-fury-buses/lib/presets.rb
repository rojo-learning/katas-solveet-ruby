
require 'json'

class Presets

  def self.load(file)
    @@presets = JSON.parse(File.open(file, 'r').read, symbolize_names: true)
    @@vehicle = eval("#{vehicle_class}.new #{seats_configuration}")

    assing_random_seats if use_random_seats
    replace_constans; self
  end

  private
    def self.replace_constans
      const_set :VEHICLE, vehicle
      const_set :OCCUPIED_SEAT_MARK, occupied_seat_mark
    end

    def self.vehicle
      @@vehicle
    end

    def self.presets
      @@presets
    end

    def self.vehicle_class
      presets[:vehicle_class]
    end

    def self.occupied_seat_mark
      presets[:occupied_seat_mark]
    end

    def self.seats_configuration
      presets[:seats_configuration]
    end

    def self.use_random_seats
      presets[:use_random_seats]
    end

    def self.min_taken_seats
      presets[:min_taken_seats]
    end

    def self.max_taken_seats
      presets[:max_taken_seats]
    end

    def self.taken_seats_number
      (min_taken_seats..max_taken_seats).to_a.shuffle.first
    end

    def self.choose_occupied_seats
      (1..vehicle.seats.size).to_a.shuffle.slice(0, taken_seats_number)
    end

    def self.assing_random_seats
      choose_occupied_seats.each { |seat_number| vehicle.assign_seat seat_number }
    end

end
