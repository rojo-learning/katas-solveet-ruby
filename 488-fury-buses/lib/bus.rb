
class Bus
  attr_reader :seats, :rows, :rows_configuration

  AVAILABLE_ROW_CONFIGS = {
    2 => { str: "[%2s]|             |[%2s]", blocks_hallway: false },
    3 => { str: "[%2s][%2s]|        |[%2s]", blocks_hallway: false },
    4 => { str: "[%2s][%2s]|   |[%2s][%2s]", blocks_hallway: false },
    5 => { str: "[%2s][%2s][%2s][%2s][%2s]", blocks_hallway: true  }
  }

  def initialize(rows_configuration)
    @rows_configuration = rows_configuration
    validate
    @seats = prepare_seats
    @rows  = arrange_seats
  end

  def seats_number
    @seats_number ||= rows_configuration.reduce(:+)
  end

  def seat_available?(seat_number)
    seats[seat_number].eql? :available
  end

  def assign_seat(seat_number)
    seats[seat_number] = :occupied
  end

  def free_seat(seat_number)
    seats[seat_number] = :available
  end

  def available_row_configs
    AVAILABLE_ROW_CONFIGS
  end

  def row_representation(row_size)
    available_row_configs[row_size][:str]
  end

  def max_row_seats
    available_row_configs.keys.max
  end

  def min_row_seats
    available_row_configs.keys.min
  end

  private
    def prepare_seats
      (1..seats_number).each_with_object({}) do
        |seat, seats| seats[seat] = :available
      end
    end

    def arrange_seats
      [].tap do |rows|
        rows_configuration.reduce(1) do |seat_number, seats_in_row|
          rows.push arrange_seats_in_row(seat_number, seats_in_row)
          seat_number + seats_in_row
        end
      end
    end

    def arrange_seats_in_row(seat_number, seats_in_row)
      [].tap { |row| seats_in_row.times { |n| row.push seat_number + n } }
    end

    def validate
      rows_configuration.each_with_index do |seats_in_row, index|
        max_seats_in_row_validation seats_in_row
        min_seats_in_row_validation seats_in_row
        hallway_transit_validation seats_in_row, index
      end
    end

    def blocks_hallway?(seats_in_row)
      available_row_configs[seats_in_row][:blocks_hallway]
    end

    def max_seats_in_row_validation(seats_in_row)
      too_many_seats_in_row(seats) if seats_in_row > max_row_seats
    end

    def min_seats_in_row_validation(seats_in_row)
      too_few_seats_in_row(seats) if seats_in_row < min_row_seats
    end

    def hallway_transit_validation(seats_in_row, index)
      if blocks_hallway?(seats_in_row) and index != rows_configuration.size.pred
        config_blocks_hallway(seats_in_row, index)
      end
    end

    def config_blocks_hallway(seats_in_row, index)
      raise ArgumentError,
        "The configuration of #{seats_in_row} seats blocks the hallway at row" \
        " #{index}."
    end

    def too_many_seats_in_row(seats_in_row)
      raise ArgumentError,
        "#{seats_in_row} seats where given, but row requires at most " \
        " #{max_row_seats} seats"
    end

    def too_few_seats_in_row(seats_in_row)
      raise ArgumentError,
        "#{seats_in_row} seats where given, but row requires at least " \
        " #{min_row_seats} seats"
    end
end
