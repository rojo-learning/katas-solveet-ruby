
require_relative 'menu'

class Main
  include Menu

  MAIN_MENU = Menu.new(
    title: "----- [[ BUS MENU ]] -----",
    options: {
      1 => { command: :assign_seat, text: 'Assign Seat'  },
      2 => { command: :free_seat,   text: 'Free Seat'    },
      3 => { command: :print_seats, text: 'Show Seats'   },
      4 => { command: :quit,        text: 'Quit Program' }
    },
    input_legend: 'Type your selection:'
  )

  def initialize(vehicle, occupied_seat_mark)
    @vehicle = vehicle
    @occupied_seat_mark = occupied_seat_mark
  end

  def start
    loop do
      MAIN_MENU.print
      puts
      exec_option(MAIN_MENU)
      puts
    end
  end

  private

    def vehicle
      @vehicle
    end

    def occupied_seat_mark
      @occupied_seat_mark
    end

    def seats
      vehicle.seats
    end

    def seat_available?(seat_number)
      vehicle.seat_available? seat_number
    end

    def request_seat_number
      require_input('Seat number:').to_i
    end

    def assign_seat
      seat_number = request_seat_number

      if seat_available?(seat_number)
        vehicle.assign_seat seat_number
      else
        puts "Seat #{seat_number} is not available."
      end
    end

    def free_seat
      seat_number = request_seat_number

      if seat_available?(seat_number)
        puts "Seat #{seat_number} is already available."
      else
        vehicle.free_seat seat_number
      end
    end

    def print_seats
      puts MAIN_MENU.title
      vehicle.rows.each { |row| puts seat_row_to_s(row) }
    end

    def seat_row_to_s(seats)
      format vehicle.row_representation(seats.size), *seats_status(seats)
    end

    def seats_status(seats_numbers)
      seats_numbers.map { |number| single_seat_status number }
    end

    def single_seat_status(seat_number)
      if vehicle.seat_available?(seat_number)
        seat_number
      else
        occupied_seat_mark
      end
    end

    def quit
      puts 'Ending program...'
      exit true
    end
end
