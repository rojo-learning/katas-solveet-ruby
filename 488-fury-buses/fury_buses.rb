#!/usr/bin/env ruby

require_relative 'lib/main'
require_relative 'lib/bus'
require_relative 'lib/presets'

Presets.load(File.expand_path 'config/presets.json')
Main.new(Presets.vehicle, Presets.occupied_seat_mark).start
