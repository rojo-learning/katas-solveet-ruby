# Solveet - Ruby

Éste repositorio contiene soluciones en Ruby a ejercicios de programación
publicados en la plataforma de la comunidad [Solveet](http://www.solveet.com).

Las soluciones propuestas y comentarios pueden revisarse en mi
[perfil de Solveet](http://www.solveet.com/akaiiro).

## Contenido

* [Desafio 488][1] **Fury Buses**: Administrador de asientos para autobuses.

---
Los programas incluidos como soluciones en éste repositorio deben ser
considerados de dominio publico.

[1]: http://www.solveet.com/exercises/Programa-de-pedido-de-asientos-en-un-bus/488
